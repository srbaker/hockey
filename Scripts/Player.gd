extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var rayNode
var spriteNode

func _ready():
	set_fixed_process(true)
	rayNode = get_node("RayCast2D")
	spriteNode = get_node("Sprite")

func _fixed_process(delta):
	var motion = Vector2()
	
	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(0, -1)
		rayNode.set_rotd(180)
		spriteNode.set_rotd(90)
	if (Input.is_action_pressed("ui_down")):
		motion += Vector2(0, 1)
		rayNode.set_rotd(0)
		spriteNode.set_rotd(-90)
	if (Input.is_action_pressed("ui_left")):
		motion += Vector2(-1, 0)
		rayNode.set_rotd(-90)
		spriteNode.set_rotd(180)
	if (Input.is_action_pressed("ui_right")):
		motion += Vector2(1, 0)
		rayNode.set_rotd(90)
		spriteNode.set_rotd(0)
	
	motion = motion.normalized() * 200 * delta
	move(motion)
